package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private ArrayList<Cat>  myCats;

	public CatManager()
	{   
	     myCats = new ArrayList<>();  //Answer to Q2
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}

	
	public void add(Cat aCat)   //Answer to Q3
	{
		myCats.add(aCat);
	}


	
	public Cat findThisCat(String name) //Answer to Q3
	{ for (Cat s : myCats) {
        if (s.getName().equals(name)) 
        {return s;}
		}return null;}


	public int countColors(String color)  //Answer to Q3
	{int count = 0;
    for (Cat s : myCats) {
    if (s.getColor().equals(color)) {
            count++;}
    }
    return count;}}